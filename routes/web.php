<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/hello',function (){
    return 'Hello Larevel';
});


Route::get('/stutent',function (){
    return 'Hello Student';
});

Route::get('/users/{email}/{name?}', function ($email = "" , $name = null ){
    return view('users', compact('name','email'));} 
);